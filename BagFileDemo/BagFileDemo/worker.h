#ifndef WORKER_H
#define WORKER_H

#include <QObject>
#include <QThread>

#include "Bagfiledemo.h"

class Worker : public QObject {
  Q_OBJECT
 public:
  explicit Worker(QObject *parent = nullptr);
    ~Worker();
    QThread mythread;
    BagFileDemo dagfiledemo;
signals:
};

#endif  // WORKER_H
