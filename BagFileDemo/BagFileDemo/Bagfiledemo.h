#ifndef BAGFILEDEMO_H
#define BAGFILEDEMO_H

#include <QFileDialog>
#include <QMainWindow>
#include <QPushButton>
#include <iostream>

#include <rosbag/bag.h>
#include <rosbag/view.h>

#include <std_msgs/Int32.h>
#include <std_msgs/String.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>

#include <sensor_msgs/PointCloud2.h>

#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;

namespace Ui {
class BagFileDemo;
}

class BagFileDemo : public QMainWindow {
  Q_OBJECT

 public:
  explicit BagFileDemo(QWidget* parent = 0);
  ~BagFileDemo();

 private slots:
  void on_filebutton_clicked();
  void on_pushButton_clicked();
  void onSavepathClicked();
  void onSaveClicked();
  void onStopSaveClicked();
public slots:
  void onWriteFile();
 public:
  void open();
  void analysisImg(const rosbag::View::iterator& iter);
  void analysisPointCloud(const rosbag::View::iterator& iter);
  void setUi();
  void open(string &str);
  BagFileDemo *daFileDemo();

 private:
  Ui::BagFileDemo* ui;

 protected:
  string m_filePath;
  string m_savePath;

  rosbag::Bag m_file;
  rosbag::View* m_view;
  vector<string> m_point;
  vector<string> m_img;

 public:
  rosbag::Bag m_writebag;

  rosbag::View::iterator m_pointCloudIter;
  rosbag::View::iterator m_leftImageIter;
  rosbag::View::iterator m_rightImageIter;

  rosbag::View::iterator GetRightImageIter();
  rosbag::View::iterator GetLeftImageIter();
  rosbag::View::iterator GetPointCloudIter();

};

#endif  // BAGFILEDEMO_H
