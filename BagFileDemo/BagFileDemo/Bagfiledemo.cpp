#include "Bagfiledemo.h"
#include <QMessageBox>
#include <boost/foreach.hpp>
#include "ui_bagfiledemo.h"
#include "worker.h"

//#define foreach BOOST_FOREACH

BagFileDemo::BagFileDemo(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::BagFileDemo) {
    ui->setupUi(this);

    m_point.push_back(string("/kitti/velo/pointcloud"));
    m_img.push_back(string("/kitti/camera_color_right/image_raw"));
    m_img.push_back(string("/kitti/camera_color_left/image_raw"));

    m_view = nullptr;

    connect(ui->Btn_SavePath, &QPushButton::clicked, this, &BagFileDemo::onSavepathClicked);
    connect(ui->Btn_Save, &QPushButton::clicked, this, &BagFileDemo::onSaveClicked);

    setUi();
}

BagFileDemo::~BagFileDemo() { delete ui; }


void BagFileDemo::on_filebutton_clicked() {
    QString curPath = QDir::currentPath();
    QString title = tr("请选择bag文件");
    QString filter = tr("bag文件(*.bag);;所有文件(*.*)");
    QString fileName = QFileDialog::getOpenFileName(this, title, curPath, filter);
    if (!fileName.isEmpty()) {
        ui->fileEdit->setText(fileName);

        m_filePath = fileName.toStdString();
    }
}

void BagFileDemo::on_pushButton_clicked() {
    if (ui->fileEdit->text().isEmpty()) {
        QMessageBox::question(this, tr("警告"), QString(tr("请先选bag文件!")),
                              QMessageBox::Yes);
        return;
    }

    m_file.open(m_filePath, rosbag::bagmode::Read);
    open();
}

void BagFileDemo::onSavepathClicked() {
    QString savePath = QFileDialog::getExistingDirectory(this, "请选择保存路径", "./");
    if(!savePath.isEmpty())
    {
        ui->Edit_SavePath->setText(savePath);
        m_savePath = savePath.toStdString();
        open(m_savePath);
    }
}

void BagFileDemo::onSaveClicked()
{
   // Worker::Worker();
}

void BagFileDemo::onStopSaveClicked()
{
    //Worker::~Worker();
}

void BagFileDemo::open()
{
    if (ui->pointcloud->isChecked() == true)
    {
        ui->image->setRowCount(0);
        ui->image->clearContents();

        m_view = new rosbag::View(m_file, rosbag::TopicQuery(m_point));
        m_pointCloudIter = m_view->begin();
        /*foreach(rosbag::MessageInstance const m, view)
    {
        sensor_msgs::PointCloud2Ptr rosPointptr =
    m.instantiate<sensor_msgs::PointCloud2>();

        if(nullptr == rosPointptr)
        {
          return;
        }

        pcl::PointCloud<pcl::PointXYZ> cloud;
        Point3D m_point;

        pcl::fromROSMsg(*rosPointptr,cloud);

        int i = 1;
        for(auto cloudIter = cloud.begin();cloudIter != cloud.end();cloudIter++)
        {
            QString m_i = QString::number(i);

            m_point.x = cloudIter->_PointXYZ::x;
            m_point.y = cloudIter->_PointXYZ::y;
            m_point.z = cloudIter->_PointXYZ::z;

            QString m_x = QString::number(m_point.x);
            QString m_y = QString::number(m_point.y);
            QString m_z = QString::number(m_point.z);

            int RowCont=ui->image->rowCount();
            ui->image->insertRow(RowCont);

            ui->image->setItem(RowCont,0,new QTableWidgetItem(m_i));
            ui->image->setItem(RowCont,1,new QTableWidgetItem(m_x));
            ui->image->setItem(RowCont,2,new QTableWidgetItem(m_y));
            ui->image->setItem(RowCont,2,new QTableWidgetItem(m_z));

            ++i;
        }

        break;
    }*/

        analysisPointCloud(m_pointCloudIter);
    }
    else if (ui->imageradio->isChecked() == true)
    {
        /* rosbag::View view(m_file, rosbag::TopicQuery(m_img));
    foreach(rosbag::MessageInstance const m, view)
    {
        sensor_msgs::ImagePtr rosImgePtr = m.instantiate<sensor_msgs::Image>();
        if(nullptr == rosImgePtr)
        {
            QMessageBox::question(this,tr("警告"),QString(tr("没有图像数据！")),QMessageBox::Yes);
            return;
        }

        //cv_bridge作用是用于ROS图像消息和opencv图像之间进行转换
        cv_bridge::CvImagePtr tempImage =
    cv_bridge::toCvCopy(rosImgePtr,sensor_msgs::image_encodings::BGR8);
        cv::Mat img = tempImage->image;
        cv::Mat rgb;

        cv::cvtColor(img,rgb,CV_BGR2RGB);

        cv::namedWindow("MyWindow", CV_WINDOW_AUTOSIZE);
        cv::imshow("MyWindow", rgb);
        break;
    }*/
        m_view = new rosbag::View(m_file, rosbag::TopicQuery(m_img));

        rosbag::View::iterator tempIter;
        for (auto iter = m_view->begin(); iter != m_view->end(); iter++) {
            tempIter = iter;
            analysisImg(tempIter);
        }
    }

    if (m_view != nullptr) {
        delete m_view;
        m_view = nullptr;
    }

    m_file.close();
}

void BagFileDemo::analysisImg(const rosbag::View::iterator &iter)
{
    sensor_msgs::ImageConstPtr rosImgePtr = (*iter).instantiate<sensor_msgs::Image>();
    if (nullptr == rosImgePtr) {
        return;
    }
    cv_bridge::CvImageConstPtr tempImage = cv_bridge::toCvCopy(rosImgePtr, sensor_msgs::image_encodings::BGR8);
    cv::Mat img = tempImage->image;
    cv::Mat rgb;

    cv::cvtColor(img, rgb, CV_BGR2RGB);

    cv::namedWindow("MyWindow", CV_WINDOW_AUTOSIZE);
    cv::imshow("MyWindow", rgb);
    cv::waitKey(5);
}

void BagFileDemo::analysisPointCloud(const rosbag::View::iterator &iter)
{
    sensor_msgs::PointCloud2ConstPtr rosPointCloudPtr = (*iter).instantiate<sensor_msgs::PointCloud2>();
    if (nullptr == rosPointCloudPtr)
    {
        return;
    }

    pcl::PointCloud<pcl::PointXYZ> cloud;

    pcl::fromROSMsg(*rosPointCloudPtr, cloud);

    int i = 1;
    for (auto cloudIter = cloud.begin(); cloudIter != cloud.end(); cloudIter++) {
        QString m_i = QString::number(i);
        QString m_x = QString::number(cloudIter->_PointXYZ::x);
        QString m_y = QString::number(cloudIter->_PointXYZ::y);
        QString m_z = QString::number(cloudIter->_PointXYZ::z);

        int RowCont = ui->image->rowCount();
        ui->image->insertRow(RowCont);

        ui->image->setItem(RowCont, 0, new QTableWidgetItem(m_i));
        ui->image->setItem(RowCont, 1, new QTableWidgetItem(m_x));
        ui->image->setItem(RowCont, 2, new QTableWidgetItem(m_y));
        ui->image->setItem(RowCont, 2, new QTableWidgetItem(m_z));

        ++i;
    }
}

void BagFileDemo::setUi() {
    ui->image->setColumnCount(4);
    ui->image->setHorizontalHeaderLabels(QStringList() << "Id"
                                         << "X"
                                         << "Y"
                                         << "Z");
    ui->image->verticalHeader()->setVisible(false);
    ui->image->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->image->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->image->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    ui->image->setShowGrid(true);
}

void BagFileDemo::open(string &str)
{
    m_writebag.open(str, rosbag::bagmode::Write);
}


void BagFileDemo::onWriteFile()
{

}

BagFileDemo *BagFileDemo::daFileDemo()
{
    return new BagFileDemo;
}
