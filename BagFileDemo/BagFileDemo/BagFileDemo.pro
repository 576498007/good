#-------------------------------------------------
#
# Project created by QtCreator 2019-06-01T21:11:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BagFileDemo
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        Bagfiledemo.cpp \
    worker.cpp

HEADERS += \
        Bagfiledemo.h \
    worker.h

FORMS += \
        bagfiledemo.ui

INCLUDEPATH += /usr/include/eigen3

unix:!macx: LIBS += -L$$PWD/../../../../../opt/ros/kinetic/lib/ -lroslib

INCLUDEPATH +=/usr/include/pcl-1.7

INCLUDEPATH += $$PWD/../../../../../opt/ros/kinetic/include
DEPENDPATH += $$PWD/../../../../../opt/ros/kinetic/include

unix:!macx: LIBS += -L$$PWD/../../../../../opt/ros/kinetic/lib/ -lrosbag

INCLUDEPATH += $$PWD/../../../../../opt/ros/kinetic/include
DEPENDPATH += $$PWD/../../../../../opt/ros/kinetic/include

unix:!macx: LIBS += -L$$PWD/../../../../../opt/ros/kinetic/lib/ -lrosbag_storage

INCLUDEPATH += $$PWD/../../../../../opt/ros/kinetic/include
DEPENDPATH += $$PWD/../../../../../opt/ros/kinetic/include

INCLUDEPATH += /usr/include/boost
LIBS += /usr/lib/x86_64-linux-gnu/libboost_*.so

LIBS += /usr/lib/x86_64-linux-gnu/libpcl_*.so
LIBS += -L/opt/ros/kinetic/lib/ -lrosbag
LIBS += -L/opt/ros/kinetic/lib/ -lrosbag_storage
LIBS += -L/opt/ros/kinetic/lib/ -lroscpp
LIBS += -L/opt/ros/kinetic/lib/ -lrosconsole
LIBS += -L/opt/ros/kinetic/lib/ -lcpp_common
LIBS += -L/opt/ros/kinetic/lib/ -lrostime
LIBS += -L/opt/ros/kinetic/lib/ -lroscpp_serialization
LIBS += -L/opt/ros/kinetic/lib/ -lrosconsole_bridge
LIBS += -L/opt/ros/kinetic/lib/ -lconsole_bridge
LIBS += -L/opt/ros/kinetic/lib/ -lroslz4

INCLUDEPATH += /opt/ros/kinetic/include/opencv-3.3.1-dev

LIBS += -L/opt/ros/kinetic/lib/ -lcv_bridge

unix:!macx: LIBS += -L$$PWD/../../../../../opt/ros/kinetic/lib/x86_64-linux-gnu/ -lopencv_imgproc3 -lopencv_core3 -lopencv_highgui3 -lopencv_imgcodecs3

INCLUDEPATH += $$PWD/../../../../../opt/ros/kinetic/lib/x86_64-linux-gnu
DEPENDPATH += $$PWD/../../../../../opt/ros/kinetic/lib/x86_64-linux-gnu



