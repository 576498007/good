/********************************************************************************
** Form generated from reading UI file 'bagfiledemo.ui'
**
** Created by: Qt User Interface Compiler version 5.9.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BAGFILEDEMO_H
#define UI_BAGFILEDEMO_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BagFileDemo
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QLineEdit *Edit_SavePath;
    QLineEdit *fileEdit;
    QSpacerItem *horizontalSpacer_5;
    QRadioButton *imageradio;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButton;
    QTableWidget *image;
    QRadioButton *pointcloud;
    QPushButton *Btn_Save;
    QPushButton *filebutton;
    QPushButton *Btn_StopSave;
    QPushButton *Btn_SavePath;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *Btn_Dialog;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *BagFileDemo)
    {
        if (BagFileDemo->objectName().isEmpty())
            BagFileDemo->setObjectName(QStringLiteral("BagFileDemo"));
        BagFileDemo->resize(583, 505);
        centralWidget = new QWidget(BagFileDemo);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        Edit_SavePath = new QLineEdit(centralWidget);
        Edit_SavePath->setObjectName(QStringLiteral("Edit_SavePath"));
        Edit_SavePath->setReadOnly(true);

        gridLayout->addWidget(Edit_SavePath, 4, 0, 1, 3);

        fileEdit = new QLineEdit(centralWidget);
        fileEdit->setObjectName(QStringLiteral("fileEdit"));
        fileEdit->setReadOnly(true);

        gridLayout->addWidget(fileEdit, 0, 1, 1, 3);

        horizontalSpacer_5 = new QSpacerItem(188, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_5, 1, 0, 1, 1);

        imageradio = new QRadioButton(centralWidget);
        imageradio->setObjectName(QStringLiteral("imageradio"));

        gridLayout->addWidget(imageradio, 1, 2, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(188, 22, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_4, 3, 0, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(187, 22, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_3, 3, 3, 1, 1);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        gridLayout->addWidget(pushButton, 3, 1, 1, 2);

        image = new QTableWidget(centralWidget);
        image->setObjectName(QStringLiteral("image"));

        gridLayout->addWidget(image, 2, 0, 1, 4);

        pointcloud = new QRadioButton(centralWidget);
        pointcloud->setObjectName(QStringLiteral("pointcloud"));
        pointcloud->setChecked(true);

        gridLayout->addWidget(pointcloud, 1, 1, 1, 1);

        Btn_Save = new QPushButton(centralWidget);
        Btn_Save->setObjectName(QStringLiteral("Btn_Save"));

        gridLayout->addWidget(Btn_Save, 5, 0, 1, 2);

        filebutton = new QPushButton(centralWidget);
        filebutton->setObjectName(QStringLiteral("filebutton"));

        gridLayout->addWidget(filebutton, 0, 0, 1, 1);

        Btn_StopSave = new QPushButton(centralWidget);
        Btn_StopSave->setObjectName(QStringLiteral("Btn_StopSave"));

        gridLayout->addWidget(Btn_StopSave, 5, 2, 1, 2);

        Btn_SavePath = new QPushButton(centralWidget);
        Btn_SavePath->setObjectName(QStringLiteral("Btn_SavePath"));

        gridLayout->addWidget(Btn_SavePath, 4, 3, 1, 1);

        horizontalSpacer_6 = new QSpacerItem(187, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_6, 1, 3, 1, 1);

        Btn_Dialog = new QPushButton(centralWidget);
        Btn_Dialog->setObjectName(QStringLiteral("Btn_Dialog"));

        gridLayout->addWidget(Btn_Dialog, 6, 0, 1, 4);

        BagFileDemo->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(BagFileDemo);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 583, 28));
        BagFileDemo->setMenuBar(menuBar);
        mainToolBar = new QToolBar(BagFileDemo);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        BagFileDemo->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(BagFileDemo);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        BagFileDemo->setStatusBar(statusBar);

        retranslateUi(BagFileDemo);

        QMetaObject::connectSlotsByName(BagFileDemo);
    } // setupUi

    void retranslateUi(QMainWindow *BagFileDemo)
    {
        BagFileDemo->setWindowTitle(QApplication::translate("BagFileDemo", "BagFileDemo", Q_NULLPTR));
        imageradio->setText(QApplication::translate("BagFileDemo", "image", Q_NULLPTR));
        pushButton->setText(QApplication::translate("BagFileDemo", "read", Q_NULLPTR));
        pointcloud->setText(QApplication::translate("BagFileDemo", "pointcloud", Q_NULLPTR));
        Btn_Save->setText(QApplication::translate("BagFileDemo", "save", Q_NULLPTR));
        filebutton->setText(QApplication::translate("BagFileDemo", "choicefile", Q_NULLPTR));
        Btn_StopSave->setText(QApplication::translate("BagFileDemo", "stopsave", Q_NULLPTR));
        Btn_SavePath->setText(QApplication::translate("BagFileDemo", "Storage Path", Q_NULLPTR));
        Btn_Dialog->setText(QApplication::translate("BagFileDemo", "Dialog", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class BagFileDemo: public Ui_BagFileDemo {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BAGFILEDEMO_H
