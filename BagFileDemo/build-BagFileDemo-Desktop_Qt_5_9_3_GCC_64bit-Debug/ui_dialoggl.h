/********************************************************************************
** Form generated from reading UI file 'dialoggl.ui'
**
** Created by: Qt User Interface Compiler version 5.9.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGGL_H
#define UI_DIALOGGL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>

QT_BEGIN_NAMESPACE

class Ui_DialogGl
{
public:

    void setupUi(QDialog *DialogGl)
    {
        if (DialogGl->objectName().isEmpty())
            DialogGl->setObjectName(QStringLiteral("DialogGl"));
        DialogGl->resize(400, 300);

        retranslateUi(DialogGl);

        QMetaObject::connectSlotsByName(DialogGl);
    } // setupUi

    void retranslateUi(QDialog *DialogGl)
    {
        DialogGl->setWindowTitle(QApplication::translate("DialogGl", "Dialog", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DialogGl: public Ui_DialogGl {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGGL_H
